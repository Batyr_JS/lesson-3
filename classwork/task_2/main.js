let hButton=document.getElementById('HouseButton');
let tButton=document.getElementById('TreeButton');
let pButton=document.getElementById('PlaneButton');

let hIconHolder=document.getElementById('houseIconHolder');
let tIconHolder=document.getElementById('treeIconHolder');
let pIconHolder=document.getElementById('planeIconHolder');
let resultHolder=document.getElementById('resultHolder');

let imgH=document.createElement('img');
let imgT=document.createElement('img');
let imgP=document.createElement('img');
let imgResult=document.createElement('img');

let i;

function setImgStyle(){
    imgH.id='iconHouse';
    imgT.id='iconTree';
    imgP.id='iconPlane';
    imgResult.id='resultImg';
}

hButton.addEventListener('click', function(){
    hIconHolder.appendChild(imgH);
         imgH.id='iconHouse';
            imgH.src='https://image.flaticon.com/icons/svg/149/149064.svg';
    
    imgH.addEventListener('click',function(){
         indexRandomiser(0,houseArray.length);
              h('resultImg',imgResult,houseArray[i]);
    });
});

tButton.addEventListener('click', function(){
    indexRandomiser(0,treeArray.length);
         tIconHolder.appendChild(imgT);
          imgT.id='iconTree';
             imgT.src='https://image.flaticon.com/icons/svg/620/620705.svg';
    
    imgT.addEventListener('click',function(){
        indexRandomiser(0,treeArray.length);
             h('resultImg',imgResult,treeArray[i]);
    });
});

pButton.addEventListener('click', function(){
    indexRandomiser(0,planeArray.length);
         pIconHolder.appendChild(imgP);
          imgP.id='iconTree';
             imgP.src='https://image.flaticon.com/icons/svg/579/579268.svg';
    
    imgP.addEventListener('click',function(){
        indexRandomiser(0,planeArray.length);
             h('resultImg',imgResult,planeArray[i]);
    });
});

function h(y,z,x){
    resultHolder.appendChild(imgResult);
    z.id=y;
    z.src=x;
}

function indexRandomiser(min,max){
    i=Math.floor(Math.random() * (max-min))+min;
}


var houseArray = [
        "https://static.pexels.com/photos/106399/pexels-photo-106399.jpeg",
        "https://i.pinimg.com/736x/7f/be/50/7fbe50ec634c65709d7fe6ac267c4e6f--large-garage-plans-house-plans-large-family.jpg",
        "https://i.ytimg.com/vi/Xx6t0gmQ_Tw/maxresdefault.jpg"
];
var planeArray = [
        "http://www.x-plane.com/wp-content/uploads/2014/08/B777-200ER.png",
        "https://media.cntraveler.com/photos/57067c1e9adc6caf5afe3f4c/master/pass/plane-landing-cr-getty-sb10062851ai-001.jpg",
        "https://media.wired.com/photos/59323264a31264584499367f/master/w_1024,c_limit/diesel-plane-inline.jpg"
];
var treeArray = [
        "https://static.pexels.com/photos/56875/tree-dawn-nature-bucovina-56875.jpeg",
        "https://c1.staticflickr.com/8/7218/7184786016_1ddab461e8_b.jpg",
        "http://www.atzmut.com/wp-content/uploads/2016/01/tree.jpg"
];

